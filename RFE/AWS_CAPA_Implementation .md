# Enhancement/Feature description 
To Introduce aws infra provider and EKS bootstrap provider in sylva eco-system. With this feature addition, it would be possible to deploy workload and/or management cluster on aws. 
Currently there is only support for rke2 and kubeadm flavor and with this enhancement ,there will be one more flavor that is EKS / EKS-A. 


# Technical implementation 
This is the proposed rough technical implementation 
- Agree on the Architecture Design for the MVP phases
- Map Architecture components with capa capabilities, identify gaps
- Address current gaps or agree with a workaround
- Introduce a unit which will deploy capa operator 
- Create templates for AWS infrastructure including managed control plane cluster 
- Introduce these template in sylva elements capi-cluster templates 
- Create corresponding values and schema to pass into the capi-cluster capa templates 
- Create AWS infrastructure and EKS cluster with the help of workload cluster creation workflow.


# How to test it

This section is optional.

We need to identify a way to test the feature, with test cases that can be run in the CI, to ensure the feature keeps running while the Sylva stack evolve.
